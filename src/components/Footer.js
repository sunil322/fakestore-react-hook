import React from "react";
import "./Footer.css";

function Footer() {
    return (
        <footer>
            <p>&copy; 2023 FakeStore.com</p>
        </footer>
    );
}

export default Footer;
