import React, { useEffect, useState } from "react";
import "./ProductDetails.css";
import {useNavigate, useParams } from "react-router-dom";
import Loader from "./Loader";
import Error from "./Error";

function ProductDetails() {

    const API_STATES_LOADING = "loading";
    const API_STATES_LOADED = "loaded";
    const API_STATES_ERROR = "error";

    const { id } = useParams("id");

    const [productDetails, setProductDetails] = useState({});

    const [apiStatus, setApiStatus] = useState(API_STATES_LOADING);

    const navigate = useNavigate();

    const [itemAddToCart,setItemAddToCart] = useState(false);
    const [itemExistInCart,setItemExistInCart] = useState(false);
    const [message,setMessage] = useState("message");

    const addToCart = (id, title, image, price) => {
        if(localStorage.getItem(`product${id}`) !==null){
            setMessage("Item already in the cart");
            setItemExistInCart(true);
            setTimeout(()=>{
                setItemExistInCart(false);
            },1000);
        }else{
            localStorage.setItem(
                `product${id}`,
                JSON.stringify({
                    id,
                    title,
                    image,
                    price,
                    quantity: 1,
                    totalPrice:price
                })
            );
            setMessage("Item added to the cart");
            setItemAddToCart(true);
            setTimeout(()=>{
                setItemAddToCart(false);
            },1000)
        }
        
    };

    const handleGoToCartClick = () =>{
        navigate('/cart');
    }

    useEffect(() => {
        fetch(`https://fakestoreapi.com/products/${id}`)
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                setProductDetails(data);
                setApiStatus(API_STATES_LOADED);
            })
            .catch((err) => {
                setApiStatus(API_STATES_ERROR);
            });
    }, [id]);

    return (
        <>
            {apiStatus === API_STATES_LOADING && <Loader />}
            {apiStatus === API_STATES_ERROR ? (
                <Error />
            ) : (
                Object.keys(productDetails).length !== 0 && (
                    <div className="product-container">
                        <div className="product-image">
                            <img src={productDetails.image} alt="product-img" />
                        </div>
                        <div className="product-details-info">
                            <p className="product-category">
                                {productDetails.category}
                            </p>
                            <h1 className="product-title">
                                {productDetails.title}
                            </h1>
                            <p className="product-description">
                                {productDetails.description}
                            </p>
                            <p className="product-price">
                                Price : $ {productDetails.price}
                            </p>
                            <p className="product-rating">
                                {" "}
                                <i className="fa-solid fa-star"></i>{" "}
                                {productDetails.rating.rate} (
                                {productDetails.rating.count})
                            </p>
                        </div>
                        <div className="btn-container">
                            <button className="add-cart" onClick={() => addToCart(productDetails.id, productDetails.title, productDetails.image,productDetails.price)}>
                                <i className="fa-sharp fa-solid fa-cart-plus"></i>{" "}
                                <span> Add to cart</span>
                            </button>
                            <button className="go-cart" onClick={handleGoToCartClick}>
                                <i className="fa-sharp fa-solid fa-cart-plus"></i>
                                <span> Go to cart</span>
                            </button>
                        </div>
                      <div style={{textAlign:'center'}}>
                        <p style={(itemAddToCart||itemExistInCart) ? {display:'block',fontWeight:'bold',padding:'0.5rem'} : {visibility:'hidden',padding:'0.5rem'}}>{message}</p>
                      </div>
                    </div>
                )
            )}
        </>
    );
}

export default ProductDetails;
