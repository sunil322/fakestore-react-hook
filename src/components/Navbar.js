import React from "react";
import "./Navbar.css";
import logo from "../images/logo.png";
import { Link } from "react-router-dom";

function Navbar() {

    return (
        <>
            <nav>
                <div className="nav-logo">
                    <Link to="/">
                        <img src={logo} alt="logo" />
                    </Link>
                </div>
                <div className="nav-menu">
                    <ul>
                        <li>
                            <Link to="/cart">
                                <i className="fa-sharp fa-solid fa-cart-plus"></i>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    );
}

export default Navbar;
