import React from "react";
import "./Error.css";

function Error() {
    return (
        <div className="error-div">
            <h1>500 Internal Server Error</h1>
            <h2>Error in Loading Products</h2>
        </div>
    );
}

export default Error;
