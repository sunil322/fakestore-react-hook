import React, { useState } from "react";

function Cart(props) {
    const [quantity, setQuantity] = useState(
        Number(props.productInfo.quantity)
    );
    const [price, setPrice] = useState(Number(props.productInfo.totalPrice));

    const incrementQuantity = (id) => {
        const productInfo = JSON.parse(localStorage.getItem(`product${id}`));
        localStorage.setItem(
            `product${id}`,
            JSON.stringify({
                ...productInfo,
                quantity: Number(productInfo.quantity) + 1,
                totalPrice: (quantity + 1) * Number(productInfo.price),
            })
        );
        setQuantity(Number(productInfo.quantity) + 1);
        setPrice(((quantity + 1) * Number(productInfo.price)).toFixed(2));
        props.changeQuantity();
    };

    const decrementQuantity = (id) => {
        const productInfo = JSON.parse(localStorage.getItem(`product${id}`));
        localStorage.setItem(
            `product${id}`,
            JSON.stringify({
                ...productInfo,
                quantity: Number(productInfo.quantity) - 1,
                totalPrice: (quantity - 1) * Number(productInfo.price),
            })
        );
        setQuantity(Number(productInfo.quantity) - 1);
        setPrice(((quantity - 1) * Number(productInfo.price)).toFixed(2));
        props.changeQuantity();
    };

    const removeItem = (id) => {
        props.removeItemFromCart(id);
    };

    return (
        <div className="cart-container">
            <div className="cart-product-image">
                <img src={props.productInfo.image} alt="product-img" />
            </div>
            <div className="cart-product-info">
                <p className="cart-title">{props.productInfo.title}</p>
                <p className="cart-quantity">
                    <button
                        disabled={quantity === 1 ? true : false}
                        onClick={() => decrementQuantity(props.productInfo.id)}
                    >
                        <i className="fa-solid fa-circle-minus"></i>
                    </button>
                    <span>{quantity}</span>
                    <button
                        disabled={quantity === 5 ? true : false}
                        onClick={() => incrementQuantity(props.productInfo.id)}
                    >
                        <i className="fa-solid fa-circle-plus"></i>
                    </button>
                </p>
                <p className="cart-price">Price : $ {price}</p>
                <button
                    className="remove-btn"
                    onClick={() => removeItem(props.productInfo.id)}
                >
                    Remove
                </button>
            </div>
        </div>
    );
}

export default Cart;
