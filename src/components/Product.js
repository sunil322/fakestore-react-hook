import React, { useState } from "react";
import "./Products.css";
import { useNavigate } from "react-router-dom";

function Product(props) {

    const navigate = useNavigate();

    const { category, id, image, price, rating, title } = props.product;

    const [itemAddToCart,setItemAddToCart] = useState(false);
    const [itemExistInCart,setItemExistInCart] = useState(false);
    const [message,setMessage] = useState("message");

    const addToCart = (id, title, image, price) => {
        if(localStorage.getItem(`product${id}`) !==null){
            setMessage("Item already in the cart");
            setItemExistInCart(true);
            setTimeout(()=>{
                setItemExistInCart(false);
            },1000);
        }else{
            localStorage.setItem(
                `product${id}`,
                JSON.stringify({
                    id,
                    title,
                    image,
                    price,
                    quantity: 1,
                    totalPrice:price
                })
            );
            setMessage("Item added to the cart");
            setItemAddToCart(true);
            setTimeout(()=>{
                setItemAddToCart(false);
            },1000)
        }
        
    };

    const handleClick = () => {
        navigate(`/${id}`);
    };

    return (
        <div className="product">
            <div className="product-img" onClick={handleClick}>
                <img src={image} alt="product" />
            </div>
            <div className="product-info" onClick={handleClick}>
                <p className="category">{category}</p>
                <p className="title">{title}</p>
                <p className="price">
                    Price : <span>${price}</span>
                </p>
                <p className="rating">
                    <span>
                        <i className="fa-solid fa-star"></i> {rating.rate} (
                        {rating.count})
                    </span>
                </p>
            </div>
            <div className="button-div">
                <button className="add-to-cart-btn" onClick={() => addToCart(id, title, image, price)}>
                    <i className="fa-sharp fa-solid fa-cart-plus"></i>{" "}
                    <span>Add to cart</span>
                </button>
                <p style={(itemAddToCart||itemExistInCart) ? {display:'block',fontWeight:'bold',padding:'0.5rem'} : {visibility:'hidden',padding:'0.5rem'}}>{message}</p>
            </div>
        </div>
    );
}

export default Product;
