import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./Cart.css";
import Cart from "./Cart";

function CartList() {
    const [cartItems, setCartItems] = useState(Object.keys(localStorage));

    const [removeProductId,setRemoveProductId] = useState("");

    const [cartTotalPrice, setCartTotalPrice] = useState(
        getTotalCartPrice().toFixed(2)
    );

    const [showPopup, setShowPopup] = useState(false);

    const confirmButtonClick = () => {
        setShowPopup(false);
        localStorage.removeItem(`product${removeProductId}`);
        setCartTotalPrice(getTotalCartPrice().toFixed(2));
        setCartItems(Object.keys(localStorage));
    };

    const cancelButtonClick = () => {
        setShowPopup(false);
    };

    const removeItemFromCart = (id) => {
        setShowPopup(true);
        setRemoveProductId(id);
    };

    function getTotalCartPrice() {
        return Object.keys(localStorage).reduce((totalPrice, product) => {
            return (
                totalPrice +
                Number(JSON.parse(localStorage.getItem(product)).totalPrice)
            );
        }, 0);
    }

    const changeQuantity = () => {
        setCartTotalPrice(getTotalCartPrice().toFixed(2));
    };

    return (
        <>
            {cartItems.length === 0 ? (
                <div className="no-items-cart-div">
                    <h1>No items in the cart</h1>
                    <Link to="/">
                        <button className="browse-products-btn">
                            Browse Products
                        </button>
                    </Link>
                </div>
            ) : (
                <>
                    {cartItems.map((product, index) => {
                        const productInfo = JSON.parse(localStorage.getItem(product));
                        return (
                            <Cart
                                key={productInfo.id}
                                productInfo={productInfo}
                                removeItemFromCart={removeItemFromCart}
                                changeQuantity={changeQuantity}
                            />
                        );
                    })}
                    <div className="cart-checkout-div">
                        <h3 className="cart-total-price">
                            Total Price : $ {cartTotalPrice}
                        </h3>
                        <button>Check out</button>
                    </div>
                    {showPopup && (
                        <div className="pop-up-div">
                            <h3>Are you sure you want to remove this item ?</h3>
                            <button
                                className="confirm-btn"
                                onClick={confirmButtonClick}
                            >
                                Confirm
                            </button>
                            <button
                                className="cancel-btn"
                                onClick={cancelButtonClick}
                            >
                                Cancel
                            </button>
                        </div>
                    )}
                </>
            )}
        </>
    );
}

export default CartList;
