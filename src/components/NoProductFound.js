import React from "react";
import "./NoProductFound.css";

function NoProductFound() {
    return (
        <div className="message-div">
            <h1>No Product Found</h1>
        </div>
    );
}

export default NoProductFound;
