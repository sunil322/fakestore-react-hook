import React, { useEffect, useState } from "react";
import "./Products.css";
import Product from "./Product";
import Loader from "./Loader";
import NoProductFound from "./NoProductFound";
import Error from "./Error";

function Products() {
    const API_STATES_LOADING = "loading";
    const API_STATES_LOADED = "loaded";
    const API_STATES_ERROR = "error";

    const [productsData, setProductsData] = useState([]);
    const [apiStatus, setApiStatus] = useState(API_STATES_LOADING);

    useEffect(() => {
        fetch("https://fakestoreapi.com/products")
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                setProductsData([...data]);
                setApiStatus(API_STATES_LOADED);
            })
            .catch((err) => {
                setApiStatus(API_STATES_ERROR);
            });
    }, []);

    return (
        <>
            {apiStatus === API_STATES_LOADING && <Loader />}
            {productsData.length === 0 && apiStatus === API_STATES_LOADED && (
                <NoProductFound />
            )}
            {apiStatus === API_STATES_ERROR ? (
                <Error />
            ) : (
                <div className="products-container">
                    {productsData.map((product) => {
                        return <Product key={product.id} product={product} />;
                    })}
                </div>
            )}
        </>
    );
}
export default Products;
