import React from "react";
import Products from "./components/Products";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import { Routes, Route } from "react-router-dom";
import ProductDetails from "./components/ProductDetails";
import "./App.css";
import CartList from "./components/CartList";

function App() {
    return (
        <div className="main-container">
            <Navbar />
            <Routes>
                <Route path="/" element={<Products />} />
                <Route path="/:id" element={<ProductDetails />} />
                <Route path="/cart" element={<CartList/>}/>
            </Routes>
            <Footer />
        </div>
    );
}

export default App;
